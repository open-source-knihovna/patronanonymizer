![logo R-Bit Technology, s.r.o.](https://gitlab.com/open-source-knihovna/SmartWithdrawals/raw/master/SmartWithdrawals/logo.png "Logo R-Bit Technology, s.r.o.")
![logo KohaCZ](https://gitlab.com/open-source-knihovna/SmartWithdrawals/raw/master/SmartWithdrawals/koha_cz.png "Logo Česká komunita Koha")

Plugin vytvořila společnost R-Bit Technology, s. r. o. ve spolupráci s českou komunitou Koha.

# Úvod

Zásuvný modul 'Anonymizace čtenářů' pro otevřený knihovní systém Koha, který je výrazným pomocníkem každé knihovny v procesu anonymizace nepoužívaných čtenářských účtů. Tuto povinnost ukládá knihovnám nařízení Evropského parlamentu a Rady (EU) 2016/679 ze dne 27. dubna 2016 o ochraně fyzických osob v souvislosti se zpracováním osobních údajů a o volném pohybu těchto údajů a o zrušení směrnice 95/46/ES, známého též pod zkratkou GDPR.

Pro skutečně snadnou práci lze nastavení vyhledávacích parametrů uložit, pojmenovat a celou předvolbu případně doplnit i delším slovním popisem. Uložené předvolby se dají snadno duplikovat a vytvářet tak velmi rychle různé varianty nastavení.

# Instalace

## Zprovoznění Zásuvných modulů

Institut zásuvných modulů umožňuje rozšiřovat vlastnosti knihovního systému Koha dle specifických požadavků konkrétní knihovny. Zásuvný modul se instaluje prostřednictvím balíčku KPZ (Koha Plugin Zip), který obsahuje všechny potřebné soubory pro správné fungování modulu.

Pro využití zásuvných modulů je nutné, aby správce systému tuto možnost povolil v nastavení.

Nejprve je zapotřebí provést několik změn ve vaší instalaci Kohy:

* V souboru koha-conf.xml změňte `<enable_plugins>0</enable_plugins>` na `<enable_plugins>1</enable_plugins>`
* Ověřte, že cesta k souborům ve složce `<pluginsdir>` existuje, je správná a že do této složky může webserver zapisovat
* Pokud je hodnota `<pluginsdir>` např. `/var/lib/koha/kohadev/plugins`, vložte následující kód do konfigurace webserveru:
```
Alias /plugin/ "/var/lib/koha/kohadev/plugins/"
<Directory "/var/lib/koha/kohadev/plugins">
  Options +Indexes +FollowSymLinks
  AllowOverride All
  Require all granted
</Directory>
```
* Načtěte aktuální konfiguraci webserveru příkazem `sudo service apache2 reload`

Jakmile je nastavení připraveno, budete potřebovat změnit systémovou konfigurační hodnotu UseKohaPlugins v administraci Kohy. Na stránce Nástroje pak najdete odkaz Zásuvné moduly. Aktuální verzi pluginu [stahujte v sekci Repository / Tags](https://gitlab.com/open-source-knihovna/patronanonymizer/tags).


Více informací, jak s nástrojem pracovat naleznete na [wiki](https://gitlab.com/open-source-knihovna/patronanonymizer/-/wikis/home)
