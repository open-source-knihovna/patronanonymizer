package Koha::Plugin::Com::RBitTechnology::PatronAnonymizer;

use Modern::Perl;
use base qw(Koha::Plugins::Base);
use utf8;
use POSIX qw(strftime);
use C4::Context;
use Koha::AdditionalContents;
use Koha::Patrons;
use Koha::List::Patron;
use Koha::Patron::Categories;
use Koha::Database;
use List::MoreUtils;

use Data::Dumper;

our $VERSION = "2.0.2";

our $metadata = {
    name            => 'Anonymizace čtenářů',
    author          => 'R-Bit Technology, s.r.o.',
    description     => 'Tento nástroj pomáhá s cílenou anonymizací, které umožňuje vybírat podle uživatelsky definovaných kritérií',
    date_authored   => '2019-12-30',
    date_updated    => '2023-11-06',
    minimum_version => '21.11',
    # maximum_version => undef,
    version         => $VERSION
};

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    ## Here, we call the 'new' method for our base class
    ## This runs some additional magic and checking
    ## and returns our actual $self
    my $self = $class->SUPER::new($args);

    return $self;
}

sub install() {
    my ( $self, $args ) = @_;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table = $self->get_qualified_table_name('predef_options');

    return  C4::Context->dbh->do( "
        CREATE TABLE IF NOT EXISTS $table_predefs (
            `predef_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(80) NOT NULL,
            `description` TEXT DEFAULT NULL,
            `date_created` DATETIME NOT NULL,
            `last_modified` DATETIME NOT NULL,
            PRIMARY KEY(`predef_id`)
        ) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_czech_ci;
        " ) && C4::Context->dbh->do( "
        CREATE TABLE IF NOT EXISTS $table (
            `predef_option_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
            `predef_id` INT( 11 ) NOT NULL,
            `variable` VARCHAR(50) NOT NULL,
            `value` TEXT NOT NULL,
            PRIMARY KEY(`predef_option_id`),
            INDEX `fk_predef_options_idx` (`predef_id` ASC),
            CONSTRAINT `fk_patronanonymizer_predef_options`
            FOREIGN KEY (`predef_id`)
            REFERENCES `$table_predefs` (`predef_id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
        ) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_czech_ci;
    " );
}

sub uninstall() {
    my ( $self, $args ) = @_;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table_options = $self->get_qualified_table_name('predef_options');

    return C4::Context->dbh->do("DROP TABLE $table_options") && C4::Context->dbh->do("DROP TABLE $table_predefs");
}

sub tool {
    my ( $self, $args ) = @_;

    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('phase') ) {;
        $self->tool_list_predefs();
    }
    elsif ( $cgi->param('phase') eq 'edit' ) {
        $self->tool_edit_predef();
    }
    elsif ( $cgi->param('phase') eq 'run' ) {
        $self->tool_get_results();
    }
    elsif ( $cgi->param('phase') eq 'delete' ) {
        $self->tool_delete_predef();
    }
    elsif ( $cgi->param('phase') eq 'duplicate' ) {
        $self->tool_duplicate_predef();
    }
    elsif ( $cgi->param('phase') eq 'share_list' ) {
        $self->tool_share_list();
    }
    elsif ( $cgi->param('phase') eq 'sql' ) {
        $self->tool_show_sql();
    }

}

sub tool_list_predefs {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-list.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my $dbh = C4::Context->dbh;
    my $table_predefs = $self->get_qualified_table_name('predefs');

    my $query = "SELECT predef_id, name, description, date_created, last_modified FROM $table_predefs;";

    my $sth = $dbh->prepare($query);
    $sth->execute();

    my @results;
    while ( my $row = $sth->fetchrow_hashref() ) {
        push( @results, $row );
    }

    $template->param(
        predefs => \@results
    );

    print $template->output();
}

sub tool_edit_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-edit.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my $options = {};
    my $predef = undef;
    my $dbh = C4::Context->dbh;
    my $query;
    my $sth;

    if ( defined $cgi->param('predef') ) {
        my $table_options = $self->get_qualified_table_name('predef_options');
        my $table_predefs = $self->get_qualified_table_name('predefs');

        $query = "SELECT predef_id, name, description FROM $table_predefs WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );
        $predef = $sth->fetchrow_hashref();

        $query = "SELECT variable, value FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );

        my %multi = map { $_ => 1 } qw(
            branches
            categories
        );

        while ( my $row = $sth->fetchrow_hashref() ) {
            if ( exists( $multi{$row->{variable}} ) ) {
                my @arr = split /,/, $row->{value};
                $options->{$row->{variable}} = \@arr;
            }
            else {
                $options->{$row->{variable}} = $row->{value};
            }
        }
    }

    # prepare data for the form
    my @branches  = Koha::Libraries->search_filtered({}, { order_by => ['branchname'], columns => [qw/branchcode branchname/] } );
    my @categories = C4::Context->only_my_library ? Koha::Patron::Categories->search_limited({ category_type => { '!=' => 'S' } }, { order_by => ['description'], columns => [qw/categorycode description/] } )
                                                  : Koha::Patron::Categories->search(        { category_type => { '!=' => 'S' } }, { order_by => ['description'], columns => [qw/categorycode description/] } );

    $template->param(
        branches => \@branches,
        categories => \@categories,
        options => $options,
        predef => $predef,
    );

    print $template->output();
}

sub execute_sql {
    my ( $self, $predefId, $returnSql ) = @_;

    my %versions = C4::Context::get_versions();
    my $kohaVer = substr($versions{'kohaVersion'}, 0, 2) . substr($versions{'kohaVersion'}, 3, 2);

    # retrieve column list
    my $dbh = C4::Context->dbh;
    my $table_options = $self->get_qualified_table_name('predef_options');

    # prepare subconditions
    my $subcond = {};
    my $query = "SELECT variable, value FROM $table_options WHERE  variable NOT LIKE 'chk_%' AND predef_id = ?;";
    my $sth = $dbh->prepare($query);
    $sth->execute($predefId);
    while ( my $row = $sth->fetchrow_hashref() ) {
        $subcond->{$row->{variable}} = $row->{value};
    }

    my @where;
    my @bindParams;
    my @havingParts;
    my @queryCols = qw( borrowernumber );
    my $period = {
        day => 1,
        month => 30,
        year => 365
    };
    my $operator = {
        avg => '=',
        min => '>=',
        max => '<='
    };

    if (0+$kohaVer < 1911) {
        push( @where, "borrowernumber NOT IN ( select guarantorid from borrowers where guarantorid IS NOT NULL )" );
    }
    else {
        push( @where, "borrowernumber NOT IN ( select guarantor_id from borrower_relationships )" );
    }

    unless ( $subcond->{sex} eq 'X' ) {
        push( @where, "borrowers.sex = ?" );
        push( @bindParams, $subcond->{sex} );
    }
    unless ( $subcond->{money} eq 'X' ) {
        my $op = $subcond->{money} eq 'N' ? 'NOT IN' : 'IN';
        push( @where, "borrowernumber $op ( select distinct borrowernumber from accountlines where amountoutstanding > 0 )" );
    }
    unless ( $subcond->{issues} eq 'X' ) {
        my $op = $subcond->{issues} eq 'N' ? 'NOT IN' : 'IN';
        push( @where, "borrowernumber $op ( select distinct borrowernumber from issues )" );
    }
    unless ( $subcond->{debarment} eq 'X' ) {
        my $op = $subcond->{debarment} eq 'N' ? 'NOT IN' : 'IN';
        push( @where, "borrowernumber $op ( select distinct borrowernumber from borrower_debarments )" );
    }
    if ( $subcond->{renew_period_length} ne '' ) {
        push( @where, "DATEDIFF(now(), dateexpiry) / ? >= ?" );
        push( @bindParams, $period->{$subcond->{renew_period_type}} );
        push( @bindParams, $subcond->{renew_period_length} );
    }

    my @branches = split(',', $subcond->{branches});
    if (scalar @branches > 0) {
        my @qMarks;
        foreach my $branch ( @branches ) {
            push( @bindParams, $branch );
            push( @qMarks, '?' );
        }
        push( @where, "borrowers.branchcode IN (" . join(',', @qMarks) . ")" );
    }
    else {
        push( @where, "1 = 0"); # branch is mandatory
    }

    my @categories = split(',', $subcond->{categories});
    if (scalar @categories > 0) {
        my @qMarks = ();
        foreach my $category ( @categories ) {
            push( @bindParams, $category );
            push( @qMarks, '?' );
        }
        push( @where, "borrowers.categorycode IN (" . join(',', @qMarks) . ")" );
    }
    else {
        push( @where, "1 = 0"); # category is mandatory
    }

    # retrieve results to display
    my $dbColumns = join(',', @queryCols);
    my $having = (scalar @havingParts > 0) ? "HAVING " . join(' AND ', @havingParts) : '';
    my $subconditions = join(' AND ', @where);

    $query = "SELECT $dbColumns "
        . " FROM borrowers "
        . " WHERE $subconditions "
        . "$having;";

    if ($returnSql) {
        return ($query, @bindParams);
    }
    else {
	    $sth = $dbh->prepare( $query );
	    for my $i (0 .. $#bindParams) {
	        $sth->bind_param($i + 1, $bindParams[$i]);
	    }
	    $sth->execute();
	
	    return ($sth);
    }
}

sub tool_show_sql {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $predefId = scalar $cgi->param('predef');

    my ($query, @bindParams) = $self->execute_sql($predefId, 1);

    my $template = $self->get_template({ file => 'tool-sql.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    $template->param(
        query => $query,
        bind_params => \@bindParams
    );

    print $template->output();
}

sub tool_get_results {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $predefId;

    if ( defined $cgi->param('save') || defined $cgi->param('save_run') ) {
        $predefId = $self->tool_save_predef();
    }

    if ( defined $cgi->param('save') ) {
        $self->tool_list_predefs();
    }
    else {
        my $template = $self->get_template({ file => 'tool-results.tt' });

        print $cgi->header(-type => 'text/html',
                           -charset => 'utf-8');

        if ( !defined $predefId && defined $cgi->param('predef') ) {
            $predefId = $cgi->param('predef');
        }

        my ($sth) = $self->execute_sql($predefId, 0);

        my @patrons;
        my @borrowernumbers;
        while ( my $row = $sth->fetchrow_hashref() ) {
            push( @borrowernumbers, $row->{borrowernumber} );
        }
        @patrons = Koha::Patrons->search( { borrowernumber => \@borrowernumbers });

        my %versions = C4::Context::get_versions();
        $template->param(
            patrons => \@patrons,
            rows => scalar @patrons,
            patron_lists => [ Koha::List::Patron::GetPatronLists() ],
            predef => $predefId,
            version => substr($versions{'kohaVersion'}, 0, 2) . substr($versions{'kohaVersion'}, 3, 2)
        );

        print $template->output();
    }

}

sub tool_save_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $dbh = C4::Context->dbh;
    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table_options = $self->get_qualified_table_name('predef_options');

    my $predef_id;
    my $query;
    my $sth;

    if ( defined $cgi->param('predef') ) {
        $query = "UPDATE $table_predefs SET name = ?, description = ?, last_modified = now() WHERE predef_id = ?;";

        $predef_id = scalar $cgi->param('predef');

        $sth = $dbh->prepare($query);
        $sth->execute(
            defined $cgi->param('predef-name') ? scalar $cgi->param('predef-name') : '(nepojmenováno)',
            defined $cgi->param('predef-descr') ? scalar $cgi->param('predef-descr') : undef,
            $predef_id
        );

        $query = "DELETE FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute($predef_id);
    }
    else {
        $query = "INSERT INTO $table_predefs (name, description, date_created, last_modified) VALUES(?, ?, now(), now());";

        $sth = $dbh->prepare($query);
        $sth->execute(
            defined $cgi->param('predef-name') ? scalar $cgi->param('predef-name') : '(nepojmenováno)',
            defined $cgi->param('predef-descr') ? scalar $cgi->param('predef-descr') : undef
        );

        $predef_id = $dbh->last_insert_id(undef, undef, $table_predefs, 'predef_id');
    }


    my @fields = qw(
        sex
        branches
        categories
        renew_period_length renew_period_type
        money
        issues
        debarment
    );
    my %multi = map { $_ => 1 } qw(
        branches
        categories
    );

    my @data;
    for my $f (@fields) {
        if ( defined $cgi->param($f) ) {
            my $value;
            if ( exists( $multi{$f} ) ) {
                my @options = $cgi->multi_param($f);
                $value = join(',', @options);
            }
            else {
                $value = scalar $cgi->param($f);
            }
            push( @data, ($predef_id, $f, $value) );
        }
    }

    $query = "INSERT INTO $table_options (predef_id, variable, value) VALUES ";
    $query .= "(?, ?, ?)," x (scalar @data / 3);
    $query =~ s/,$/;/g;

    $sth = $dbh->prepare($query);
    $sth->execute( @data );

    return $predef_id;
}

sub tool_delete_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    if ( defined $cgi->param('predef') ) {
        my $dbh = C4::Context->dbh;
        my $table_predefs = $self->get_qualified_table_name('predefs');

        my $query = "DELETE FROM $table_predefs WHERE predef_id = ?;";

        my $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );
    }

    $self->tool_list_predefs();
}

sub tool_duplicate_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    if ( defined $cgi->param('predef') ) {
        my $dbh = C4::Context->dbh;
        my $table_predefs = $self->get_qualified_table_name('predefs');
        my $table_options = $self->get_qualified_table_name('predef_options');

        my $query = "INSERT INTO $table_predefs (name, description, date_created, last_modified) SELECT CONCAT('(kopie ', ? ') ', name), description, now(), now() FROM $table_predefs WHERE predef_id = ?;";
        my $sth = $dbh->prepare($query);
        $sth->execute( $cgi->param('predef'), $cgi->param('predef') );
        my $new_predef_id = $dbh->last_insert_id(undef, undef, $table_predefs, 'predef_id');

        $query = "INSERT INTO $table_options (predef_id, variable, value) SELECT ?, variable, value FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( $new_predef_id, $cgi->param('predef') );
    }

    $self->tool_list_predefs();
}

sub tool_share_list {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    if ( defined $cgi->param('patron_list_id') ) {
        Koha::List::Patron::ModPatronList( { patron_list_id => scalar $cgi->param('patron_list_id'), shared => 1 } );
    }

    binmode STDOUT, ":encoding(UTF-8)";
    print $cgi->header(
        -type => 'application/json',
        -charset => 'UTF-8'
    );
}

sub cron {
    my ( $self, $args ) = @_;
    my $patron_list_id = $args->{'patron_list_id'};
    my $predefId = $args->{'predef_id'};
    my $verbose = $args->{'verbose'};
    my $msg_position = $args->{'msg_position'};
    my $msg_author = $args->{'msg_author'};
    my $msg_branchcode = $args->{'msg_branchcode'} ? $args->{'msg_branchcode'} : undef;
    my $dbh = C4::Context->dbh;
    my $sth;
    my $query;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    $query = "SELECT predef_id, name, description FROM $table_predefs WHERE predef_id = ?;";
    $sth = $dbh->prepare($query);
    $sth->execute( $predefId );
    my $predef_row = $sth->fetchrow_hashref();

    # cannot use simpler GetPatronLists as we have no context in cron
    my $schema = Koha::Database->new()->schema();
    my @patron_lists = $schema->resultset('PatronList')->search({ patron_list_id => $patron_list_id });
    my $list = $patron_lists[0];
    my @patrons = Koha::Patrons->search( { borrowernumber => $msg_author });

    if ($predef_row && $list && @patrons) {

        my @existing = $list->patron_list_patrons;
        $sth = $self->execute_sql($predefId, 0);
        my @borrowernumbers;
        while ( my $row = $sth->fetchrow_hashref() ) {
            push( @borrowernumbers, $row->{borrowernumber} );
        }
        my @patrons_in_list = Koha::List::Patron::AddPatronsToList( { list => $list, borrowernumbers => \@borrowernumbers } );

        my %exist = map { $_->borrowernumber->borrowernumber => 1 } @existing;
        my %found = map { $_->borrowernumber->borrowernumber => 1 } @patrons_in_list;

        my (@deleted, @added);
        foreach my $borrowernumber ( @borrowernumbers ) {
            push (@added, $borrowernumber) unless defined $exist{$borrowernumber};
        }
        foreach my $patron ( @existing ) {
            push (@deleted, $patron->patron_list_patron_id) unless defined $found{$patron->borrowernumber->borrowernumber};
        }

        Koha::List::Patron::DelPatronsFromList( { list => $list, patron_list_patrons => \@deleted } );

        if ($verbose) {
            print "Deleted " . (scalar @deleted) . " and added " . (scalar @added) . " patrons to list " . $list->name . " filtered according to saved predefinition " . $predef_row->{name} . "\n";
        }

        if ((scalar @deleted > 0) || (scalar @added > 0)) {
	        my $additional_content = Koha::AdditionalContent->new(
	            {
	                category       => 'news',
	                code           => 'tmp_code',
	                location       => 'staff_only',
	                title          => 'Anonymizace čtenářů',
	                content        => '<div class="alert alert-danger" style="text-align: left;">Byl aktualizován anonymizační seznam <a href="/cgi-bin/koha/patron_lists/list.pl?patron_list_id=' . $list->patron_list_id . '">' . $list->name . '</a> ' .
                      'v souladu s předvolbou <a href="/cgi-bin/koha/plugins/run.pl?class=Koha::Plugin::Com::RBitTechnology::PatronAnonymizer&method=tool&phase=edit&predef=' . $predef_row->{predef_id} . '">#' . $predef_row->{predef_id} . ($predef_row->{name} ? (' - ' . $predef_row->{name}) : '') . '</a><br>' .
                      '<ul style="border-top: 1px solid #e0c726; padding-top: 0.5em; margin: 0.5em 0;"><li>přidaných čtenářů: ' . (scalar @added) . '</li><li>odebraných čtenářů: ' . (scalar @deleted) . '</li><li>aktuální počet: ' . $list->patron_list_patrons->count . '</li></ul>' .
                      '<a href="/cgi-bin/koha/tools/cleanborrowers.pl?step=2&patron_list_id=' . $list->patron_list_id . '&checkbox=borrower" class="btn btn-default btn-sm"><i class="fa fa-user-times"></i> Provést anonymizaci</a></div>',
	                lang           => 'default',
	                expirationdate => undef,
	                published_on   => strftime("%Y-%m-%d", localtime),
	                number         => $msg_position,
	                branchcode     => $msg_branchcode,
	                borrowernumber => $msg_author,
	            }
	        )->store;
	        eval {
	            $additional_content->store;
	            $additional_content->discard_changes;
	            my $code = 'News_' . $additional_content->idnew;
	            $additional_content->code($code)->store;
	        };
	        unless ($@) {
	            logaction('NEWS', 'ADD' , undef, sprintf("%s|%s|%s|%s", $additional_content->code, $additional_content->title, $additional_content->lang, $additional_content->content))
	                if C4::Context->preference("NewsLog");
	        }
        	
#            add_opac_new(
#                {
#                    title          => 'Anonymizace čtenářů',
#                    content        => '<div class="alert alert-danger" style="text-align: left;">Byl aktualizován anonymizační seznam <a href="/cgi-bin/koha/patron_lists/list.pl?patron_list_id=' . $list->patron_list_id . '">' . $list->name . '</a> ' .
#                      'v souladu s předvolbou <a href="/cgi-bin/koha/plugins/run.pl?class=Koha::Plugin::Com::RBitTechnology::PatronAnonymizer&method=tool&phase=edit&predef=' . $predef_row->{predef_id} . '">#' . $predef_row->{predef_id} . ($predef_row->{name} ? (' - ' . $predef_row->{name}) : '') . '</a><br>' .
#                      '<ul style="border-top: 1px solid #e0c726; padding-top: 0.5em; margin: 0.5em 0;"><li>přidaných čtenářů: ' . (scalar @added) . '</li><li>odebraných čtenářů: ' . (scalar @deleted) . '</li><li>aktuální počet: ' . $list->patron_list_patrons->count . '</li></ul>' .
#                      '<a href="/cgi-bin/koha/tools/cleanborrowers.pl?step=2&patron_list_id=' . $list->patron_list_id . '&checkbox=borrower" class="btn btn-default btn-sm"><i class="fa fa-user-times"></i> Provést anonymizaci</a></div>',
#                    lang           => 'koha',
#                    number         => $msg_position,
#                    branchcode     => $msg_branchcode,
#                    borrowernumber => $msg_author,
#                }
#            );
        }
    }
}

1;
