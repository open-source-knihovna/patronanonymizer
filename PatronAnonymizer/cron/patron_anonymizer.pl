#!/usr/bin/perl

#  This script tries to find patrons suitable for the anonymization. Found patrons
#  are added to a given patron list.
#
#  This script is meant to be run nightly out of cron.

# Copyright 2000-2002 Katipo Communications
# Copyright 2020 R-Bit Technology, s.r.o.
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;

use CGI qw ( -utf8 );

use Koha::Plugins::Handler;

use C4::Context;
use C4::Log;
use Getopt::Long;
use Carp;
use Koha::Plugins::Handler;

my $cgi = new CGI;

my $class  = 'Koha::Plugin::Com::RBitTechnology::PatronAnonymizer';
my $method = 'cron';

my $help;
my $verbose;
my $patron_list_id;
my $predef_id;
my $msg_position;
my $msg_author;

GetOptions(
    'h|help'          => \$help,
    'v|verbose'       => \$verbose,
    'list-id=i'       => \$patron_list_id,
    'predef-id=i'     => \$predef_id,
    'msg-position=i'  => \$msg_position,
    'msg-author=i'    => \$msg_author,
);
my $usage = << 'ENDUSAGE';

This script tries to find patrons suitable for the anonymization. Found patrons
are added to a given patron list.

This script has the following parameters:
    -h --help:        this message
    -v:               little more verbose output
    --list-id:        ID of target patron list (see table patron_lists)
    --predef-id:      ID of saved search criteria (see column ID in the list of predefinitions in PatronAnonymizer plugin interface)
    --msg-position:   position where the result message appears in left panel
    --msg-author:     borrowernumber used for message authoring

ENDUSAGE

if ($help || !$patron_list_id || !$predef_id || !$msg_position || !$msg_author) {
    print $usage;
    exit;
}

cronlogaction();

my $plugin = Koha::Plugins::Handler->run( {
    class => $class,
    method => $method,
    cgi => $cgi,
    params => {
        patron_list_id => $patron_list_id,
        predef_id => $predef_id,
        verbose => $verbose,
        msg_author => $msg_author,
        msg_position => $msg_position
    }
} );
